import React, { useState } from 'react'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from './pages/Home/Home';
import UpcommingSessions from './components/UpcommingSessions';
import RegisterNewRdv from './components/RegisterNewRdv';
import ShowInstructors from './components/ShowInstructors/ShowInstructors';
import Student from './components/Student/Student';
import MonitorStudent from './components/MonitorStudent/MonitorStudent';
import Layout from './components/Layout/Layout';
import CardMonitor from './components/CardMonitor/CardMonitor';
import Map from './components/Map';
import StudentPage from './pages/Student';
import Monitors from './pages/Monitors';
import { AuthContext } from './lib/contexts/AuthContext';
import './styles/_reset.scss';
import './styles/_global.scss';
import EditRdv from './components/editRdv/EditRdv';

const App = () => {
  const [user, setUser] = useState(false);

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
        <Layout>
          <Routes>
            <Route index={true} element={<Home/>}/>
            <Route path="register" element={<RegisterNewRdv />}/>
            <Route path="sessions" element={<UpcommingSessions/>} />
            <Route path="show" element={<ShowInstructors/>}/>
            <Route path="map/:id" element={<Map/>}/>
            <Route path="student/:id" element={<Student />} />
            <Route path="monitor/:id" element={<CardMonitor />} />
            <Route path="student" element={<StudentPage/>}/>
            <Route path="monitors" element={<Monitors/>}/>
            <Route path="editRdv" element={<EditRdv />} />
            <Route path="monitor-student/:id" element={<MonitorStudent />} />
            <Route path="editRdv/:id" element={<EditRdv />}/>
          </Routes>
        </Layout>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};
export default App;
