import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import StudentCompetences from "../StudentCompetences/StudentCompetences";
import styles from "./MonitorStudent.module.scss"

const Student = () => {
    const { id } = useParams()
    const [student, setStudent] = useState(null)
    useEffect(() => {
        fetch('/api/users/' + id)
            .then(res => res.json())
            .then(data => setStudent(data))
    }, [id])
    return <div className={styles.wrapper}> {student && <>
        <div className={styles.wrapper__header}>
            <h1 className={styles.wrapper__studentName}>{student.firstName} {student.lastName}</h1>
            <div className={styles.wrapper__profile}>
                <img className={styles.wrapper__image} src={`${process.env.PUBLIC_URL}/moniteurs/${student.image}`} alt="yes" />
                <div className={styles.wrapper__time}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="50px" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <p className={styles.wrapper__timeSpent}>19h</p>
                </div>
            </div>
            <div className={styles.wrapper__contact}>
                <h2 className={styles.wrapper__titreContact}>Contact</h2>
                <div className={styles.wrapper__contactWay}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="25px" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                    </svg>
                    <a href={"mailto:" + student.email}>{student.email}</a>
                </div>
                <div className={styles.wrapper__contactWay}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="25px" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
                    </svg>
                    <a href="tel:0750276914">07.50.27.69.14</a>
                </div>
            </div>
        </div>
        <div className={styles.wrapper__progressbar}>
            <h2 className={styles.wrapper__titre}>Compétences</h2>
            <progress className={styles.wrapper__progress} id="file" max="90" value={student.competences.length * 10}> 70% </progress>
        </div>
        <StudentCompetences idUser={id} />
    </>}</div >

}

export default Student