import React, {useEffect, useState} from 'react';
import styles from './Reservation.module.scss';

const ShowInstructors = (props) => {


    const {
        idUser
    } = props

    const [reservations, setReservations] = useState([]);

    const [users, setUsers] = useState([])
    useEffect(() => {
        fetch('/api/users')
            .then(res => res.json())
            .then(data => setUsers(data))
    }, []);

    const [places, setPlaces] = useState([])
    useEffect(() => {
        fetch('/api/places')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, []);

    const options = {year: 'numeric', month: 'long', day: 'numeric' };
    const opt_weekday = { weekday: 'long' };
    function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    useEffect(() => {
        fetch('/api/sessions?studentUserId=' + idUser)
            .then(res => res.json())
            .then(data => setReservations(data.slice(0,3)))
    }, []);

    return (
      <ul className={styles.list}>
        {reservations.map(reservation =>
          <li className={styles.item} key={reservation.id}>
            <p className={styles.duree}>{new Date(reservation.dateEnd).toLocaleTimeString().slice(0, 2) - new Date(reservation.dateStart).toLocaleTimeString().slice(0, 2)}h</p>
            <p className={styles.date}>{toTitleCase(new Date(reservation.dateEnd).toLocaleDateString("fr-FR", opt_weekday))} {new Date(reservation.dateStart).toLocaleTimeString("fr-FR", options).slice(0, -15)}</p>
            <p className={styles.heure}>{new Date(reservation.dateStart).toLocaleTimeString().slice(0, 2)}h à {new Date(reservation.dateEnd).toLocaleTimeString().slice(0, 2)}h</p>
            <p className={styles.instructeur}>avec {users.filter(user => user.id === reservation.instructorUserId).map(user => user.firstName + ' ' + user.lastName.toUpperCase())}</p>
            <p className={styles.lieu}>{places.filter(place => place.id === reservation.placeId).map(place => place.name)}</p>
          </li>)}
      </ul>
    );
}

export default ShowInstructors