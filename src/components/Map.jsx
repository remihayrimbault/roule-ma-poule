import React, {useState, useEffect} from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import { useParams } from 'react-router-dom'


const Map = (props) => {

    const {
        idPlaces,
        heightPlace,
        widthPlace
    } = props

    const [places, setPlaces] = useState([])

    useEffect(() => {
        fetch('/api/places/')
            .then(res => res.json())
            .then(data => setPlaces(Array.from(Object.values(data))))
    }, []);

    const findPlace = (id) => {
        for(let i = 0; i < places.length; i += 1) {
            if (id === places[i].id) {
                return places[i];
            }
        }
    }


    return  <div>
                {
                    places[0] ?
                    <MapContainer center={places[0].position} zoom={13} style={{height:heightPlace, width:widthPlace}}>
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.jawg.io/{z}/{x}/{y}.png?access-token=nxTRozBCxNvtReYA1kG2v75CyxPuVJ11PRpUHPBm09v520nfnYmcDFYkkGS4dJBm"
                        />
                        {idPlaces.map(idPlace =>
                        <Marker position={findPlace(idPlace).position}>
                            <Popup>
                                {findPlace(idPlace).name}
                            </Popup>
                        </Marker>)}
                    </MapContainer>
                    :
                    ''
                }
            </div>
}

export default Map