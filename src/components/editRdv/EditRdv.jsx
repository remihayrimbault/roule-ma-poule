import React, { useEffect, useState } from "react";
import styles from './EditRdv.module.scss';
import Map from "../Map";
import { useParams, Link } from "react-router-dom";
import arrow from '../../assets/icons/arrowOrange.svg';

const EditRdv = (props) => {

    const { idSession } = useParams();

    const [session, setSession] = useState([]);
    const [places, setPlaces] = useState([]);
    const [placeId, setPlaceId] = useState();
    const [isResponsive, setIsResponsive] = useState();

    const checkPlaces = () => {
        fetch('/api/places')
            .then(res => res.json())
            .then(dataPlaces => setPlaces(dataPlaces))    
    }

    useEffect(() => {
        fetch('/api/sessions?id=' + idSession)
            .then(res => res.json())
            .then(data => {
                setSession(data)
                setPlaceId(data.idPlace);
            })
        checkPlaces()  
        if (window.innerWidth < 1024) {
            setIsResponsive(true)
        } else {
            setIsResponsive(false)
        }  
    }, [idSession])

    return <div className={styles.wrapper}>
        <div className={styles.content}>
            <h1 className={styles.title}>Modifier un rdv</h1>
            <form className={styles.form}>
                <label for="time">Date et heure :</label>
                <input name="time" type="datetime-local" value={session.dateStart}></input>
                <label for="duration">Durée :</label>
                <select name="duration" id="duration">
                    <option value="1">1 heure</option>
                    <option value="2">2 heures</option>
                    <option value="3" selected>3 heures</option>
                    <option value="4">4 heures</option>
                </select>
                <label for="place">Lieu de départ</label>
                <select name="place" id="place" value={placeId}>
                    {places.map(place =>
                        <option value={place.id} key={place.id}>{place.name}</option>)}
                </select>
                <button>Enregistrer</button>
            </form>
        </div>
        <Map idPlaces={[1]} heightPlace={isResponsive ? '30vh' : '70vh'} widthPlace={isResponsive ? '100vw' : '50vw'}></Map>
        <Link to="/student"><img className={styles.arrow} src={arrow} alt="" /></Link>
    </div >

}

export default EditRdv