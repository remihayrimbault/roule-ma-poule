import { useContext, useRef, useState } from 'react';
import Popup from 'react-customizable-popup';
import { Link, useNavigate } from 'react-router-dom';
import LoginForm from '../LoginForm/LoginForm';
import RegisterForm from '../RegisterForm/RegisterForm';
import logo from '../../assets/icons/logo.svg';
import { AuthContext } from '../../lib/contexts/AuthContext';
import useIntersection from '../../lib/hooks/useIntersection';
import styles from './Navbar.module.scss';

const Navbar = () => {
  const { user, setUser } = useContext(AuthContext);
  const loginPopup = useRef(null);
  const registerPopup = useRef(null);
  const [scrolled, setScrolled] = useState(false);
  const navigate = useNavigate();
  const intersection = useIntersection(
    100,
    () => setScrolled(true),
    () => setScrolled(false),
  );

  const handleLogin = () => {
    registerPopup.current.close();
    loginPopup.current.open();
  };

  const handleRegister = () => {
    loginPopup.current.close();
    registerPopup.current.open();
  };

  const getUser = async () => {
    const response = await fetch('/api/auth/me', {
      method: 'GET',
    });
    if (response.ok) {
      const data = await response.json();
      setUser(data);
      navigate(`/${data.role === 'instructor' ? 'monitor' : 'student'}`);
    }
  };

  const pushLoginUser = async (credentials) => {
    await fetch('/api/auth/login', {
      method: 'post',
      headers: {
        'Authorization': 'Basic ' + btoa(credentials.email + ':' + credentials.password)
      },
      body: JSON.stringify(credentials)
    })
      .then(response => response.json())
      .then(data => {
        if (data?.msg === 'success') {
          getUser();
        } else {
          alert('Mauvais indentifiant ou mot de passe.');
        }
      });
  };

  const pushNewUser = async (credentials) => {
    await fetch('/api/users', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify({
        email: credentials.email,
        role: credentials.role,
        password: credentials.password,
      })
    })    
      .then(response => response.json())
      .then(data => {
        if (data?.msg === 'success') {
          getUser();
        } else {
          alert('Une erreur s\'est prduite.');
        }
      });
  }

  const pushLogoutUser = () => {
    fetch('/api/auth/logout', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
    })    
      .then(response => response.json())
      .then(data => {
        if (data?.msg === 'success') {
          setUser(null);
          navigate('/');
        } else {
          alert('Une erreur s\'est prduite.');
        }
      }); 
  }

  return (
    <>
      <nav className={`${styles.navbar} ${scrolled && styles.scrolled}`}>
        <Link className={styles.logoContainer} to={user ? user.role === 'instructor' ? '/monitor' : '/student' : '/'}>
          <img className={styles.logo} src={logo} alt="" />
          <h1 className={styles.hero}>Roule ma poule</h1>
        </Link>
        {user ? (
          <div className={styles.pictureContainer}>
            <button
              className={styles.link}
              onClick={pushLogoutUser}
            >
              Se déconnecter
            </button>
            <img className={styles.picture} src={`${process.env.PUBLIC_URL}/moniteurs/${user.image}`} alt="" />
          </div>
        ) : (
          <>
            <div className={styles.login}>
              <Popup
                toggler={(
                  <button className={styles.link}>Se connecter</button>
                )}
                className={styles.popup}
                ref={loginPopup}
                position="modal"
              >
                <h2 className={styles.title}>Connexion</h2>
                <LoginForm onSubmit={pushLoginUser} changeHasAccount={handleRegister} />
              </Popup>
              <p>/</p>
              <Popup
                toggler={(
                  <button className={styles.link}>S'inscrire</button>
                )}
                className={styles.popup}
                ref={registerPopup}
                position="modal"
              >
                <h2 className={styles.title}>Inscription</h2>
                <RegisterForm onSubmit={pushNewUser} changeHasAccount={handleLogin} />
              </Popup>
            </div>
          </>
        )}
      </nav>
      {intersection}
    </>
  );
};

export default Navbar;
