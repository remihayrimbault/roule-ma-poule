import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import styles from "./CardMonitor.module.scss";
import demiStar from '../../assets/icons/demistar.svg';
import fullStar from '../../assets/icons/fullstar.svg';
import voidStar from '../../assets/icons/voidstar.svg';
import arrow from '../../assets/icons/arrowOrange.svg';

const CardMonitor = () => {

    const [monitor, setMonitor] = useState({});
    const [sessions, setSessions] = useState({});
    const [places, setPlaces] = useState({});
    const { id } = useParams();

    const calcNote = (notes) => {
      let res = 0;
      let array = Array.from(Object.values(notes));
      for (let i = 0; i < array.length; i += 1) {
          res += array[i];
      }
      return (res/array.length)
  }

    const checkSessions = () => {
        fetch('/api/sessions?instructorUserId=' + id)
            .then(res => res.json())
            .then(dataSession => setSessions(dataSession))    
    }

    const checkPlaces = () => {
        fetch('/api/places')
            .then(res => res.json())
            .then(dataPlaces => setPlaces(dataPlaces))    
    }

    const searchPlaces = (id) => {
        for (let i = 0; i < places.length; i += 1) {
            if (places[i].id === id) {
                return places[i].name
            }
        }
    }

    useEffect(() => {
        fetch('/api/users/' + id)
            .then(res => res.json())
            .then(data => {
              setMonitor({
                ...data,
                note: calcNote(data.notes),
              })
            })
        checkSessions()
        checkPlaces()
    }, [id])

    return <div className={styles.wrapper}>
        <Link to={'/student'}><img className={styles.arrow} src={arrow} alt="" /></Link>
        <div className={styles.container}>
          <img className={styles.image} src={`${process.env.PUBLIC_URL}/moniteurs/${monitor.image}`} alt="yes"/>
          <div className={styles.avisContainer}>
            <h1>{monitor.firstName} {monitor.lastName}</h1>
            <h2>Avis :</h2>
            <ul className={styles.stars}>
              {[...Array(5)].map((x, index) => {
                let src = '';
                if (monitor.note <= index) {
                  src = voidStar;
                } else if (monitor.note > index + 0.5) {
                  src = fullStar;
                } else {
                  src = demiStar;
                }
                return (
                  <li key={index}>
                    <img src={src} alt="" />
                  </li>
                );
              })}
            </ul>
          </div>
        </div>  
        <div className={styles.coursContainer}>
          <h2 className={styles.subtitle}>Ses prochains cours</h2>
          <ul>{Array.from(sessions)
              .sort((a, b) => a.timestamp - b.timestamp)
              .slice(0, 3)
              .map(session => <li className={styles.cours} key={session.id}>
                  <h3>Le {parseInt(new Date(session.dateStart).getDate()) < 10 ? ('0'+(new Date(session.dateStart).getDate())) : (new Date(session.dateStart).getDate())}/{parseInt(new Date(session.dateStart).getMonth()) < 10 ? ('0'+(new Date(session.dateStart).getMonth())) : (new Date(session.dateStart).getMonth())}/{new Date(session.dateStart).getFullYear()}</h3>
                  <p className={styles.place}>{searchPlaces(session.placeId)}</p>
                  <p className={styles.time}>à {parseInt(new Date(session.dateStart).getHours()) < 10 ? ('0'+(new Date(session.dateStart).getHours())) : (new Date(session.dateStart).getHours())}h{parseInt(new Date(session.dateStart).getMinutes()) < 10 ? ('0'+(new Date(session.dateStart).getMinutes())) : (new Date(session.dateStart).getMinutes())}</p>
                  <a className={styles.button} href={'/login'}>Voir</a>
              </li>)}
          </ul>
        </div>
    </div >

}

export default CardMonitor