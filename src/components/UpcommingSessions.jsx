import React, { useState, useEffect } from 'react'

const UpcommingSessions = (props) => {

    const [sessions, setSessions] = useState([])
    useEffect(() => {
        fetch('/api/sessions')
            .then(res => res.json())
            .then(data => setSessions(data))
    }, []);

    const [users, setUsers] = useState([])
    useEffect(() => {
        fetch('/api/users')
            .then(res => res.json())
            .then(data => setUsers(data))
    }, []);

    const [places, setPlaces] = useState([])
    useEffect(() => {
        fetch('/api/places')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, []);



    // config pour le format de la date
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const opt_weekday = { weekday: 'long' };
    function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }



    // recherche des sessions libres (sans personne inscrite)
    const freeSessions = []
    sessions.filter(session => !session.studentUserId).map(session =>
        freeSessions.push(session)
    )



    return <div>
        {
            freeSessions.map(session =>
                <div>
                    <p>{users.filter(user => user.id === session.instructorUserId).map(user => user.firstName + ' ' + user.lastName.toUpperCase())}</p>
                    <p>{new Date(session.dateStart).toLocaleTimeString().slice(0, 2)}h - {new Date(session.dateEnd).toLocaleTimeString().slice(0, 2)}h le {toTitleCase(new Date(session.dateEnd).toLocaleDateString("fr-FR", opt_weekday))} {new Date(session.dateStart).toLocaleTimeString("fr-FR", options).slice(0, -10)}</p>
                    <p>{places.filter(place => place.id === session.placeId).map(place => place.name)}</p>
                </div>
            )
        }
    </div>




}

export default UpcommingSessions