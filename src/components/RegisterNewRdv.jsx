import React, {useEffect, useState} from 'react'

const RegisterNewRdv = () => {

    const [places, setPlaces] = useState([]);
    const [date, setDate] = useState('');
    const [duration, setDuration] = useState(0);
    const [choosePlace, setChoosePlace] = useState(99);

    const pushNewRdv = () => {
        let newHours = new Date(date).getHours() + parseInt(duration);
        let newDate = new Date(date).setHours(newHours);
        const data = {
            placeId: parseInt(choosePlace),
            dateStart: Date.parse(date)/1000,
            dateEnd: newDate/1000,
            instructorUserId: 8 /*à modifier en récupérant l'id du prof connecté*/,
            studentUserId: null
        }

        fetch('/api/sessions', {
            method: 'post',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        })    
            .then(response => response.json())
            .then(data => console.log(data));
    }

    useEffect(() => {
        fetch('/api/places')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, []);

    return <main>
        <form>
            <input type="datetime-local" onChange={e => setDate(e.target.value)} />
            <select name="duration" id="duration" onChange={e => setDuration(e.target.value)}>
                <option>Choisir une durée</option>
                <option value="1">1 heure</option>
                <option value="2">2 heures</option>
                <option value="3">3 heures</option>
                <option value="4">4 heures</option>
            </select>
            <select name="place" id="place" onChange={e => setChoosePlace(e.target.value)}>
                <option>Choisir un lieu de départ</option>
                {places.map(place =>
                    <option value={place.id}>{place.name}</option>)}
            </select>
        </form>
        <button onClick={pushNewRdv}>Enregistrer</button>
    </main>
}

export default RegisterNewRdv