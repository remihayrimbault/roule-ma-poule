import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import StudentCompetences from "../StudentCompetences/StudentCompetences";
import styles from "./Student.module.scss"

const Student = () => {
    const { id } = useParams()
    const [student, setStudent] = useState(null)
    // const [competences, setCompetences] = useState(null)
    useEffect(() => {
        fetch('/api/users/' + id)
            .then(res => res.json())
            .then(data => setStudent(data))
    }, [id])
    // useEffect(() => {
    //     fetch('/api/competences')
    //         .then(res => res.json())
    //         .then(data => setCompetences(data))
    // }, [])
    // (student && competences && student.competences.map(cmpId => <li>{competences[cmpId - 1].label}</li>))
    return <div className={styles.wrapper}> {student && <>
        <div className={styles.wrapper__header}>
            <div className={styles.wrapper__profile}>
                <img className={styles.wrapper__image} src={`${process.env.PUBLIC_URL}/moniteurs/${student.image}`} alt="yes" />
                <div className={styles.wrapper__time}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="50px" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <p className={styles.wrapper__timeSpent}>19h</p>
                </div>
            </div>
            <div className={styles.wrapper__contact}>
                <h2 className={styles.wrapper__titreContact}><svg xmlns="http://www.w3.org/2000/svg" width="25px" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                </svg>Contact</h2>

            </div>
            <div className={styles.wrapper__nameCpt}>
                <h1 className={styles.wrapper__studentName}>{student.firstName} {student.lastName}</h1>
                <progress className={styles.wrapper__progress} id="file" max="90" value={student.competences.length * 10}> 70% </progress>
            </div>
        </div>
        <h2 className={styles.wrapper__titre}>Mes compétences</h2>
        <StudentCompetences idUser={id} />
    </>}</div >

}

export default Student
