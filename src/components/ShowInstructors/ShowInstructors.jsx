import React, {useEffect, useState} from 'react';
import fullStar from '../../assets/icons/fullstar.svg';
import demiStar from '../../assets/icons/demistar.svg';
import voidStar from '../../assets/icons/voidstar.svg';
import styles from './ShowInstructors.module.scss';

const ShowInstructors = ({ limit }) => {

    const [instructors, setInstructors] = useState([]);

    const calcNote = (notes) => {
        let res = 0;
        let array = Array.from(Object.values(notes));
        for (let i = 0; i < array.length; i += 1) {
            res += array[i];
        }
        return (res/array.length)
    }

    // Lien pour obtenir tt les rdv d'un instructeur
    // http://localhost:8080/api/sessions?instructorUserId=8
    
    useEffect(() => {
        fetch('/api/users?role=instructor')
            .then(res => res.json())
            .then((data) => {
              const newInstructors = data.slice(0, limit).map((instructor) => (
                {
                  ...instructor,
                  note: calcNote(instructor.notes),
                }
              ));
              setInstructors(newInstructors);
            });
    }, [limit]);

    return (
      <ul className={styles.list}>
        {instructors.map((instructor) => (
          <li className={styles.instructor} key={instructor.id}>
            <img className={styles.image} src={`${process.env.PUBLIC_URL}/moniteurs/${instructor.image}`} alt="yes"/>
            <p className={styles.name}>{instructor.firstName} {instructor.lastName}</p>
            <ul className={styles.stars}>
              {[...Array(5)].map((x, index) => {
                let src = '';
                if (instructor.note <= index) {
                  src = voidStar;
                } else if (instructor.note > index + 0.5) {
                  src = fullStar;
                } else {
                  src = demiStar;
                }
                return (
                  <li key={index}>
                    <img src={src} alt="" />
                  </li>
                );
              })}
            </ul>
          </li>
        ))}
      </ul>
    );
}

export default ShowInstructors