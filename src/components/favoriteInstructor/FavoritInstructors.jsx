import React, {useState, useEffect} from 'react'
import fullStar from '../../assets/icons/fullstar.svg';
import demiStar from '../../assets/icons/demistar.svg';
import voidStar from '../../assets/icons/voidstar.svg';
import styles from './FavoritInstructors.module.scss';
import { Link } from "react-router-dom";

const FavoritInstructors = (props) => {

    const {
        idUser
    } = props

    const calcNote = (notes) => {
        let res = 0;
        let array = Array.from(Object.values(notes));
        for (let i = 0; i < array.length; i += 1) {
            res += array[i];
        }
        return (res/array.length)
    }

    const [student, setStudent] = useState([])
    useEffect(() => {
        fetch('/api/users/' + idUser)
            .then(res => res.json())
            .then(data => setStudent(data))
    }, [idUser]);

    const [instructors, setInstructors] = useState([])
    useEffect(() => {
        fetch('/api/users/')
            .then(res => res.json())
            .then(data => {
                const newInstructors = data.map((instructor) => (
                    {
                      ...instructor,
                      note: calcNote(instructor.notes),
                    }
                  ));
                  setInstructors(newInstructors);
            })
    }, []);


    // recherche de tous les instructeurs favori de l'élève
    const favoritInstructors = []
    instructors.filter(instructor => student.favoriteInstructorsId.includes(instructor.id)).map(instructor => 
        favoritInstructors.push(instructor)
    )


    return  <ul className={styles.list}>
    {favoritInstructors.map((instructor) => (
      <Link to={'/monitor/'+instructor.id} key={instructor.id}>
        <li className={styles.instructor}>
        <img className={styles.image} src={`${process.env.PUBLIC_URL}/moniteurs/${instructor.image}`} alt="yes"/>
        <p className={styles.name}>{instructor.firstName} {instructor.lastName}</p>
        <ul className={styles.stars}>
          {[...Array(5)].map((x, index) => {
            let src = '';
            if (instructor.note <= index) {
              src = voidStar;
            } else if (instructor.note > index + 0.5) {
              src = fullStar;
            } else {
              src = demiStar;
            }
            return (
              <li key={index}>
                <img src={src} alt="" />
              </li>
            );
          })}
        </ul>
      </li>
      </Link>
    ))}
  </ul>
}

export default FavoritInstructors