import React, { useEffect, useState } from "react";
import styles from "./StudentCompetences.module.scss"

const StudentCompetences = props => {
    const [student, setStudent] = useState(null)
    const [competences, setCompetences] = useState(null)
    const [categories, setCategories] = useState(null)
    useEffect(() => {
        fetch('/api/users/' + props.idUser)
            .then(res => res.json())
            .then(data => setStudent(data))
    }, [props.idUser])

    useEffect(() => {
        fetch('/api/competences')
            .then(res => res.json())
            .then(data => setCompetences(data))
    }, [])

    useEffect(() => {
        fetch('/api/categoriesCompetences')
            .then(res => res.json())
            .then(data => setCategories(data))
    }, [])

    const hasCpt = id => {
        for (let i = 0; i < student.competences.length; i++) {
            if (student.competences[i] === id) {
                return true
            }
        }
    }
    return <div className={styles.competences}>{student && competences && categories && <>
        {categories.map(categorie =>
            <ul className={styles.competences__list}>
                <h3 className={styles.competences__category}>{categorie.label}</h3>
                {competences.filter(cpt => cpt.categoryId === categorie.id).map(cpt =>
                    <li className={styles.competences__cpt}>
                        <input type={"checkbox"} checked={hasCpt(cpt.id)} />
                        <label htmlFor="1" nameClass={styles.competences__label}>{cpt.label}</label>
                    </li>)}
            </ul>
        )}
    </>}
    </div>
}
export default StudentCompetences