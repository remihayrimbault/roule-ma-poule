import { useEffect, useRef } from 'react';

const useIntersection = (
  past,
  enable,
  disable,
) => {
  const toWatch = useRef(null);

  useEffect(() => {
    const toWatchCurrent = toWatch.current;

    const observer = new IntersectionObserver((entries) => {
      if (entries[0].boundingClientRect.y < 0) {
        enable();
      } else {
        disable();
      }
    });

    if (toWatchCurrent) {
      observer.observe(toWatchCurrent);
    }

    return () => {
      if (toWatchCurrent) {
        observer.unobserve(toWatchCurrent);
      }
    };
  }, [enable, disable, toWatch]);

  return (
    <div
      ref={toWatch}
      style={{
        position: 'absolute',
        width: '1px',
        height: '1px',
        top: `${past}px`,
        left: '0',
      }}
    />
  );
};

export default useIntersection;