import Reservation from '../components/reservation/Reservation';
import FavoriteIntructor from '../components/favoriteInstructor/FavoritInstructors';
import styles from '../styles/pages/Student.module.scss';
import StudentCompetences from '../components/StudentCompetences/StudentCompetences';

const Student = () => {
  

  return (
    <>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes réservations :</h2>
        <Reservation idUser={11} />
      </section>
      <div className={styles.link}><span className={styles.line}></span><a className={styles.text}>Réserver un cours <img className={styles.icon} src={`${process.env.PUBLIC_URL}/icon-chevron-rights.png`} alt="chevron" /></a></div>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes moniteurs favortis : </h2>
        <FavoriteIntructor idUser={11} />
      </section>
      <div className={styles.link}><span className={styles.line}></span><a className={styles.text}>Voir d’autres moniteurs <img className={styles.icon} src={`${process.env.PUBLIC_URL}/icon-chevron-rights.png`} alt="chevron" /></a></div>
      <section className={styles.content}>
        <h2 className={styles.subtitle}>Mes compétences : </h2>
        <StudentCompetences idUser={11} />
      </section>
    </>
  );
};

export default Student;
