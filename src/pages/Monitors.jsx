import styles from '../styles/pages/Monitors.module.scss';
import { useEffect, useState } from 'react';
import fullStar from '../assets/icons/fullstar.svg';
import demiStar from '../assets/icons/demistar.svg';
import voidStar from '../assets/icons/voidstar.svg';


const Monitor = () => {
    const [instructors, setInstructors] = useState([]);
    const idMonitor = [3, 9];

    const calcNote = (notes) => {
        let res = 0;
        let array = Array.from(Object.values(notes));
        for (let i = 0; i < array.length; i += 1) {
            res += array[i];
        }
        return (res / array.length)
    }

    const handleClick = (e) => {
        let present = false;
        for(let i = 0; i < idMonitor.length ; i += 1) {
            
            if(idMonitor[i] == e.target.dataset.idmonitor){
                present = true
            }
        }
        if (present === true ){
            idMonitor.splice(idMonitor.indexOf(e.target.dataset.idmonitor),idMonitor.indexOf(e.target.dataset.idmonitor)+1)
            console.log(idMonitor)
        } else {
            idMonitor.push(parseInt(e.target.dataset.idmonitor))
            console.log(idMonitor)
        }
    }

    useEffect(() => {
        fetch('/api/users?role=instructor')
            .then(res => res.json())
            .then((data) => {
                const newInstructors = data.map((instructor) => (
                    {
                        ...instructor,
                        note: calcNote(instructor.notes),
                    }
                ));
                setInstructors(newInstructors);
            });
    }, []);


    return (
        <>
            <header className={styles.header}>
                <div className={styles.text}>
                    <h2 className={styles.title}>Les moniteurs</h2>
                    <h3 className={styles.subtitle}>Les moniteurs indépendants de Roule ma poule sont à votre disposition pour vous aider à obtenir votre permis.</h3>
                </div>
            </header>
            <main className={styles.main}>
                <ul className={styles.list}>
                    {instructors.map((instructor) => (
                        <li className={styles.instructor} key={instructor.id}>
                            <img className={styles.image} src={`${process.env.PUBLIC_URL}/moniteurs/${instructor.image}`} alt="yes" />
                            <div className={styles.desc}>
                                <div>
                                    <p className={styles.name}>{instructor.firstName} {instructor.lastName}</p>
                                    <ul className={styles.stars}>
                                        {[...Array(5)].map((x, index) => {
                                            let src = '';
                                            if (instructor.note <= index) {
                                                src = voidStar;
                                            } else if (instructor.note > index + 0.5) {
                                                src = fullStar;
                                            } else {
                                                src = demiStar;
                                            }
                                            return (
                                                <li key={index}>
                                                    <img src={src} alt="" />
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                                <img onClick={e => handleClick(e)} data-idmonitor={instructor.id}  className={styles.heart} src={(idMonitor.includes(instructor.id))?`${process.env.PUBLIC_URL}/heart-fill.png`: `${process.env.PUBLIC_URL}/heart-border.png`} alt="heart" />
                            </div>
                        </li>
                    ))}
                </ul>
            </main>
        </>
    );
};

export default Monitor;