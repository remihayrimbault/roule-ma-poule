import { Link } from 'react-router-dom';
import arrowIcon from '../../assets/icons/arrow.svg';
import image from '../../assets/images/home-image.png';
import mapImage from '../../assets/images/map.svg';
import progressImage from '../../assets/images/progress.svg';
import ShowInstructors from '../../components/ShowInstructors/ShowInstructors';
import { useRef } from 'react';
import styles from './Home.module.scss';

const Home = () => {
  const main = useRef(null);

  const scrollToSection = () => {
    if (main.current) {
      window.scroll(0, main.current.offsetTop - 32);
    }
  };

  return (
    <>
      <header className={styles.header}>
        <div className={styles.text}>
          <h2 className={styles.title}>Réservez vos cours de conduite facilement.</h2>
          <h3 className={styles.subtitle}>Je suis un <Link className={styles.link} to="/">élève</Link>, ou un <Link className={styles.link} to="/">moniteur</Link>.</h3>
        </div>
        <img className={styles.image} src={image} alt="" />
        <button className={styles.button} onClick={scrollToSection}>
          <img src={arrowIcon} alt="" />
          <p>Découvrez le concept</p>
        </button>
      </header>
      <main className={styles.main} ref={main}>
        <section className={styles.section}>
          <h2 className={styles.title}>Choisissez facilement vos points de rendez-vous</h2>
          <img className={styles.image} src={mapImage} alt="" />
        </section>
        <section className={styles.section}>
          <h2 className={styles.title}>Suivez votre progression et laissez-vous guider</h2>
          <img className={styles.image} src={progressImage} alt="" />
        </section>
        <section className={styles.section}>
          <h2 className={styles.title}>Les moniteurs sont à votre disposition</h2>
          <ShowInstructors limit={4} />
        </section>
        <Link className={styles.button} to="/monitors">Voir plus de moniteurs</Link>
      </main>
    </>
  );
};

export default Home;
